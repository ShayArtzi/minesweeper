/*
 TODO:
 -----
 X Do not allow clicking to a marked square
 - Add win display
 */
var boardConfig = {
    size: 10, /* will be doubled! */
    mines: 10,
    boardSize: "300px"
};
var colArray = [];
var flatArray = [];
var mines = [];
var counter = {
    revealed: 0,
    minesFoundOrSpeculated: 0
};
var win = false;
var debug = false;

var gameContainer = document.getElementById("gameContainer");
var mineCounter = document.getElementById("mineCounter");

setupBoard();

function setupBoard(){
    drawBoard();
    spreadMines();
    spreadClues();
    coverTheBoard();
    updateMineCouner();
}

function drawBoard(){

    gameContainer.style.width = boardConfig.boardSize;
    gameContainer.style.height = boardConfig.boardSize;

    for(var i = 0; i < boardConfig.size; i++){
        var col = document.createElement("div");
        col.className = "col";
        gameContainer.appendChild(col);

        var squareArray = [];
        colArray.push(squareArray);
        for(var j = 0; j < boardConfig.size; j++){
            var node = createSquare(i, j);
            col.appendChild(node);
            squareArray.push(node);
            flatArray.push(node);
        }
    }
}

function createSquare(x, y){
    var node = document.createElement("div");
    node.className = "square";
    node.id = x + "-" + y;
    node.addEventListener("click", clickSquare);
    node.addEventListener("contextmenu", rightClickSquare);
    node.mineSweeper = {
        isMine: false,
        clue: 0,
        rowIndex: x,
        colIndex: y,
        isRevealed: false,
        isMarked: false
    };
    return node;
}

function spreadMines(){
    var flatArrayTemp = flatArray.slice(0); //clone

    for(var i = 0; i < boardConfig.mines; i++){
        var rand = Math.round(Math.random()*(flatArrayTemp.length - 1) );
        var element = flatArrayTemp[rand];
        element.mineSweeper.isMine = true;
        mines.push(element);
        flatArrayTemp.splice(rand, 1);
    }
}

function spreadClues(){
    mines.forEach(function(element){
        spreadClue( getSquare(element, -1, -1) ); //Upper left
        spreadClue( getSquare(element, -1,  0) ); //Upper mid
        spreadClue( getSquare(element, -1,  1) ); //Upper right

        spreadClue( getSquare(element,  0, -1) ); //Left
        spreadClue( getSquare(element,  0,  1) ); //Right

        spreadClue( getSquare(element,  1, -1) ); //Lower left
        spreadClue( getSquare(element,  1,  0) ); //Lower mid
        spreadClue( getSquare(element,  1,  1) ); //Lower right

    });

    function spreadClue(element){
        if ( !element || element.mineSweeper.isMine){
            //do nothing...
        }else {
            element.mineSweeper.clue = element.mineSweeper.clue + 1;
        }
    }
}

function coverTheBoard(){
    flatArray.forEach( function(element){
        var inner = createInnerSquare(element);
        element.appendChild(inner);

    } );
}

function createInnerSquare(element){
    var inner = document.createElement("div");
    inner.className = "innerSquare";
    inner.mineSweeper = {
        behind: element
    };
    inner.style.backgroundColor = "gray";
    inner.style.width = "100%";
    inner.style.height = "100%";
    inner.style.cursor = "pointer";

    return inner;
}


function updateMineCouner(num){
    if (num){
        counter.minesFoundOrSpeculated = counter.minesFoundOrSpeculated + num;
    }
    mineCounter.innerHTML = counter.minesFoundOrSpeculated + "/" + mines.length;
}

function getSquare(element, colOffset, rowOffset){
    try{
        return colArray[element.mineSweeper.rowIndex + colOffset][element.mineSweeper.colIndex + rowOffset];
    }catch(e){
        //do nothing!
    }
}

function clickSquare(e){
    var front = e.target;
    var behind = e.target.mineSweeper.behind;

    if (typeof behind === 'undefined'){
        behind = front;
        front = front.childNodes[0];
    }

    if (behind.mineSweeper.isRevealed || behind.mineSweeper.isMarked){
        return;
    }

    behind.mineSweeper.isRevealed = true;
    counter.revealed = counter.revealed + 1;



    if (counter.revealed + mines.length == flatArray.length){
        if (!win){
            win = true;
        }
    }

    updateSquareView(behind);
}

function overlayBoard(){
    var overlayElement = document.createElement("div");
    if(win){
        overlayElement.style.backgroundColor = "blue";
    }else{
        overlayElement.style.backgroundColor = "black";
    }
    overlayElement.style.opacity = "0.5";
    overlayElement.style.height = gameContainer.offsetHeight + "px";
    overlayElement.style.width = gameContainer.offsetWidth + "px";
    overlayElement.style.position = "absolute";
    gameContainer.appendChild(overlayElement);
}

function rightClickSquare(e){
    e.preventDefault();

    var front = e.target;
    var behind = e.target.mineSweeper.behind;

    if (typeof behind === 'undefined'){
        behind = front;
        front = front.childNodes[0];
    }

    if (behind.mineSweeper.isRevealed){
        return;
    }

    if (behind.mineSweeper.isMarked){
        updateMineCouner(-1);
    }else{
        updateMineCouner(+1);

    }
    behind.mineSweeper.isMarked = !behind.mineSweeper.isMarked;

    updateSquareView(behind);

}

function traverseBlanks(element){
    var squaresAround = [
        getSquare(element, -1, -1), /* Upper left */
        getSquare(element, -1,  0), /* Upper mid */
        getSquare(element, -1,  1), /* Upper right */

        getSquare(element,  0, -1), /* Left */
        getSquare(element,  0,  1), /* Right */

        getSquare(element,  1, -1), /* Lower left */
        getSquare(element,  1,  0), /* Lower mid */
        getSquare(element,  1,  1)  /* Lower right */
    ];

    for(var i = 0; i < squaresAround.length; i++){
        if (typeof squaresAround[i] !== 'undefined' &&
            /*squaresAround[i].mineSweeper.clue == 0 &&*/
            !squaresAround[i].mineSweeper.isMine &&
            !squaresAround[i].mineSweeper.isRevealed){

            debugPrint(squaresAround[i].id);
            var fakeEvent = {
                target: squaresAround[i].childNodes[0]
            };
            debugPrint(fakeEvent);
            clickSquare(fakeEvent);
        }
    }

}

function updateSquareView(squareElement){
    var frontElement = squareElement.childNodes[0];

    if(debug){
        frontElement.title = JSON.stringify(squareElement.mineSweeper);
    }else{
        frontElement.title = "";
    }

    if (squareElement.mineSweeper.isRevealed){

        frontElement.removeAttribute("style");
        squareElement.style.backgroundColor = "transparent";

        if (squareElement.mineSweeper.isMine){
            //hit a mine
            debugPrint("MINE!");
            frontElement.innerHTML = "X";
            frontElement.style.color = "red";

            overlayBoard();
        }else if (squareElement.mineSweeper.clue > 0){
            //hit a clue
            debugPrint("clue: " + squareElement.mineSweeper.clue);
            frontElement.innerHTML = squareElement.mineSweeper.clue;
        }else{
            //hit blank
            debugPrint("blank");
            frontElement.style.width = "100%"; //make empty inner large so the tooltip is shown
            frontElement.style.height = "100%";
            traverseBlanks(squareElement);
        }

        if(win){
            debugPrint("WIN!");
            overlayBoard();
        }
    }else{
        if (squareElement.mineSweeper.isMarked){
            frontElement.removeAttribute("style");
            frontElement.innerHTML = "X";
            squareElement.style.backgroundColor = "gray";
        }else{
            frontElement.innerHTML = "";
        }

        if (debug && squareElement.mineSweeper.isMine && !squareElement.mineSweeper.isMarked){
            frontElement.innerHTML = "@";
        }
    }

}

function toggleDebug(cb){
    debug = cb.checked;

    flatArray.forEach( function(element){
        updateSquareView(element);
    } );
}

function debugPrint(msg){
    if (debug){
        console.log(msg);
    }
}