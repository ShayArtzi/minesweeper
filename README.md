# README #

This is a simple Mine Sweeper game implementation written is pure JS, with no external libs or dependencies. 

## Demo ##
Here is a live version: [https://kipodopik.com/minesweeper/](https://kipodopik.com/minesweeper/)

## Features ##
- Fully functional game
- Configurable size of board and number of mines
- Debug mode:
   - Mines are marked with @ symbol.
   - A tooltip showing the square state is shown when hovering over the board.

## License ##
The code is released under the MIT License (found in the main source folder).